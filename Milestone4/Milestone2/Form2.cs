﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        private void Btn_openFile_Click(object sender, EventArgs e)
        {
            try
            {
                // StreamReader to getFile
                string getFile;
                StreamReader inputFile;
                inputFile = File.OpenText("Inventory.txt");

                lb_inventory.Items.Clear();

                while (!inputFile.EndOfStream)
                {
                    getFile = inputFile.ReadLine();

                    lb_inventory.Items.Add(getFile);
                }

                // Close the file
                inputFile.Close();          
            }

            catch (Exception ex)
            {
                // Display an error message
                MessageBox.Show(ex.Message);
            }
        }

        private void L_add_Click(object sender, EventArgs e)
        {
            // Open form1
            btn_search frm = new btn_search();
            frm.Show();
        }

        private void Btn_delete_Click(object sender, EventArgs e)
        {
            // Delete inventory
            if (lb_inventory.SelectedItems.Count != 0)
            {
                while (lb_inventory.SelectedIndex != -1)
                {
                    lb_inventory.Items.RemoveAt(lb_inventory.SelectedIndex);
                }

            }
        }

        private void Btn_save_Click(object sender, EventArgs e)
        {
            // Create a StreamWriter to create text that displays the Product name, sku, brand and etc...
            StreamWriter newFile;
            newFile = File.AppendText("Inventory.txt");
            newFile.Close();

            // Display a Messagebox
            MessageBox.Show("The Inventory has been updated and saved");
        }

        private void Btn_exit2_Click(object sender, EventArgs e)
        {
            // Exit the form
            this.Close();
        }

        private void Btn_search_Click(object sender, EventArgs e)
        {
            //Variables int index and string input
            int index;
            string input;
            //Use variable input as an InputBox.
            input = (string)(Interaction.InputBox("Enter an item to search:"));
            // Use variable index to hold the value of variable input and will find the content of it
            index = System.Convert.ToInt32(lb_inventory.FindString(input));
            // If index variable will not be equal to the Items inside of the ListBox it will display "Item is found"

            if (index != ListBox.NoMatches)
            {
                MessageBox.Show("The Item Is in the Inventory!");
            }
            //otherwise, it will display "not found".
            else
            {

                MessageBox.Show("Item not found!");
            }
        }
    }
}
   


