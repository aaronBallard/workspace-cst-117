﻿namespace Milestone2
{
    partial class btn_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_name = new System.Windows.Forms.Label();
            this.l_sku = new System.Windows.Forms.Label();
            this.l_brand = new System.Windows.Forms.Label();
            this.l_qnty = new System.Windows.Forms.Label();
            this.l_retail = new System.Windows.Forms.Label();
            this.l_price = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_sku = new System.Windows.Forms.TextBox();
            this.tb_brand = new System.Windows.Forms.TextBox();
            this.tb_qnty = new System.Windows.Forms.TextBox();
            this.tb_retail = new System.Windows.Forms.TextBox();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_view = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // l_name
            // 
            this.l_name.AutoSize = true;
            this.l_name.Location = new System.Drawing.Point(46, 50);
            this.l_name.Name = "l_name";
            this.l_name.Size = new System.Drawing.Size(100, 17);
            this.l_name.TabIndex = 0;
            this.l_name.Text = "Product Label:";
            // 
            // l_sku
            // 
            this.l_sku.AutoSize = true;
            this.l_sku.Location = new System.Drawing.Point(102, 100);
            this.l_sku.Name = "l_sku";
            this.l_sku.Size = new System.Drawing.Size(44, 17);
            this.l_sku.TabIndex = 1;
            this.l_sku.Text = "Sku#:";
            // 
            // l_brand
            // 
            this.l_brand.AutoSize = true;
            this.l_brand.Location = new System.Drawing.Point(96, 150);
            this.l_brand.Name = "l_brand";
            this.l_brand.Size = new System.Drawing.Size(50, 17);
            this.l_brand.TabIndex = 2;
            this.l_brand.Text = "Brand:";
            // 
            // l_qnty
            // 
            this.l_qnty.AutoSize = true;
            this.l_qnty.Location = new System.Drawing.Point(85, 200);
            this.l_qnty.Name = "l_qnty";
            this.l_qnty.Size = new System.Drawing.Size(65, 17);
            this.l_qnty.TabIndex = 3;
            this.l_qnty.Text = "Quantity:";
            // 
            // l_retail
            // 
            this.l_retail.AutoSize = true;
            this.l_retail.Location = new System.Drawing.Point(102, 250);
            this.l_retail.Name = "l_retail";
            this.l_retail.Size = new System.Drawing.Size(48, 17);
            this.l_retail.TabIndex = 4;
            this.l_retail.Text = "Retail:";
            // 
            // l_price
            // 
            this.l_price.AutoSize = true;
            this.l_price.Location = new System.Drawing.Point(106, 300);
            this.l_price.Name = "l_price";
            this.l_price.Size = new System.Drawing.Size(44, 17);
            this.l_price.TabIndex = 5;
            this.l_price.Text = "Price:";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(185, 44);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(441, 22);
            this.tb_name.TabIndex = 6;
            // 
            // tb_sku
            // 
            this.tb_sku.Location = new System.Drawing.Point(185, 94);
            this.tb_sku.Name = "tb_sku";
            this.tb_sku.Size = new System.Drawing.Size(441, 22);
            this.tb_sku.TabIndex = 7;
            // 
            // tb_brand
            // 
            this.tb_brand.Location = new System.Drawing.Point(185, 144);
            this.tb_brand.Name = "tb_brand";
            this.tb_brand.Size = new System.Drawing.Size(441, 22);
            this.tb_brand.TabIndex = 8;
            // 
            // tb_qnty
            // 
            this.tb_qnty.Location = new System.Drawing.Point(185, 194);
            this.tb_qnty.Name = "tb_qnty";
            this.tb_qnty.Size = new System.Drawing.Size(441, 22);
            this.tb_qnty.TabIndex = 9;
            // 
            // tb_retail
            // 
            this.tb_retail.Location = new System.Drawing.Point(185, 244);
            this.tb_retail.Name = "tb_retail";
            this.tb_retail.Size = new System.Drawing.Size(441, 22);
            this.tb_retail.TabIndex = 10;
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(185, 294);
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(441, 22);
            this.tb_price.TabIndex = 11;
            // 
            // btn_save
            // 
            this.btn_save.AutoSize = true;
            this.btn_save.Location = new System.Drawing.Point(185, 364);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 27);
            this.btn_save.TabIndex = 12;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.Btn_save_Click);
            // 
            // btn_view
            // 
            this.btn_view.AutoSize = true;
            this.btn_view.Location = new System.Drawing.Point(363, 364);
            this.btn_view.Name = "btn_view";
            this.btn_view.Size = new System.Drawing.Size(75, 27);
            this.btn_view.TabIndex = 13;
            this.btn_view.Text = "View";
            this.btn_view.UseVisualStyleBackColor = true;
            this.btn_view.Click += new System.EventHandler(this.Btn_view_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.AutoSize = true;
            this.btn_exit.Location = new System.Drawing.Point(550, 364);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 27);
            this.btn_exit.TabIndex = 14;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.Btn_exit_Click);
            // 
            // btn_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_view);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.tb_price);
            this.Controls.Add(this.tb_retail);
            this.Controls.Add(this.tb_qnty);
            this.Controls.Add(this.tb_brand);
            this.Controls.Add(this.tb_sku);
            this.Controls.Add(this.tb_name);
            this.Controls.Add(this.l_price);
            this.Controls.Add(this.l_retail);
            this.Controls.Add(this.l_qnty);
            this.Controls.Add(this.l_brand);
            this.Controls.Add(this.l_sku);
            this.Controls.Add(this.l_name);
            this.Name = "btn_search";
            this.Text = "Search";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_name;
        private System.Windows.Forms.Label l_sku;
        private System.Windows.Forms.Label l_brand;
        private System.Windows.Forms.Label l_qnty;
        private System.Windows.Forms.Label l_retail;
        private System.Windows.Forms.Label l_price;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_view;
        private System.Windows.Forms.Button btn_exit;
        public System.Windows.Forms.TextBox tb_name;
        public System.Windows.Forms.TextBox tb_sku;
        public System.Windows.Forms.TextBox tb_brand;
        public System.Windows.Forms.TextBox tb_qnty;
        public System.Windows.Forms.TextBox tb_retail;
        public System.Windows.Forms.TextBox tb_price;
    }
}

