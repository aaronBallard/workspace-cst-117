﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_First_Project
{
    public partial class backPoker : Form
    {
        public backPoker()
        {
            InitializeComponent();
        }

        // Show the text in the messageButton
        private void MessageButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World");
        }

        // Show the text in button 1
        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is text from clicking on the Button");
        }

        // show the text in click button
        private void ClickButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thank you for clicking this Button!!!");
        }

        private void Lable_payRate_Click(object sender, EventArgs e)
        {

        }

        private void Lable_hoursWorked_Click(object sender, EventArgs e)
        {

        }

        private void Label_president_Click(object sender, EventArgs e)
        {
        }

        // show the answer for the btn_answer
        private void Btn_answer_Click(object sender, EventArgs e)
        {
            label_empty.Text = ("President Roosevelt");
        }

        //show the text for button 3 for the German word good morning
        private void Button3_Click(object sender, EventArgs e)
        {
            label_empty2.Text = "Guten Morgen";
        }

        // show the text for the Italian word good morning
        private void Btn_italian_Click(object sender, EventArgs e)
        {
            label_empty2.Text = "Buongiorno";
        }

        // show the text for the btn_spanish button word good morning
        private void Btn_spanish_Click(object sender, EventArgs e)
        {
            label_empty2.Text = "Buenos Dias";
        }

        // show the text for the pictrure box dragon when clicked
        private void PictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("A fearsome Dragon");
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        // show the back of the poker card when clicked
        private void Btn_backPoker_Click(object sender, EventArgs e)
        {
            backCard.Visible = true;
            acePoker.Visible = false;
        }

        private void AcePoker_Click(object sender, EventArgs e)
        {
            
        }

        // show the face of the poker card when clicked
        private void Btn_face_Click(object sender, EventArgs e)
        {
            acePoker.Visible = true;
            backCard.Visible = false;
        }

        private void Btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
