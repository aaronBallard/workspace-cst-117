﻿namespace My_First_Project
{
    partial class backPoker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageButton = new System.Windows.Forms.Button();
            this.clickButton2 = new System.Windows.Forms.Button();
            this.clickButton1 = new System.Windows.Forms.Button();
            this.label_president = new System.Windows.Forms.Label();
            this.label_empty = new System.Windows.Forms.Label();
            this.btn_answer = new System.Windows.Forms.Button();
            this.label_goodMorning = new System.Windows.Forms.Label();
            this.label_empty2 = new System.Windows.Forms.Label();
            this.btn_italian = new System.Windows.Forms.Button();
            this.btn_spanish = new System.Windows.Forms.Button();
            this.German = new System.Windows.Forms.Button();
            this.acePoker = new System.Windows.Forms.PictureBox();
            this.backCard = new System.Windows.Forms.PictureBox();
            this.dragon = new System.Windows.Forms.PictureBox();
            this.btn_backPoker = new System.Windows.Forms.Button();
            this.btn_face = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.acePoker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragon)).BeginInit();
            this.SuspendLayout();
            // 
            // messageButton
            // 
            this.messageButton.Location = new System.Drawing.Point(307, 42);
            this.messageButton.Name = "messageButton";
            this.messageButton.Size = new System.Drawing.Size(107, 54);
            this.messageButton.TabIndex = 0;
            this.messageButton.Text = "Display Message";
            this.messageButton.UseVisualStyleBackColor = true;
            this.messageButton.Click += new System.EventHandler(this.MessageButton_Click);
            // 
            // clickButton2
            // 
            this.clickButton2.Location = new System.Drawing.Point(571, 42);
            this.clickButton2.Name = "clickButton2";
            this.clickButton2.Size = new System.Drawing.Size(107, 54);
            this.clickButton2.TabIndex = 1;
            this.clickButton2.Text = "Click Button";
            this.clickButton2.UseVisualStyleBackColor = true;
            this.clickButton2.Click += new System.EventHandler(this.Button1_Click);
            // 
            // clickButton1
            // 
            this.clickButton1.Location = new System.Drawing.Point(31, 42);
            this.clickButton1.Name = "clickButton1";
            this.clickButton1.Size = new System.Drawing.Size(107, 54);
            this.clickButton1.TabIndex = 2;
            this.clickButton1.Text = "Click Button";
            this.clickButton1.UseVisualStyleBackColor = true;
            this.clickButton1.Click += new System.EventHandler(this.ClickButton1_Click);
            // 
            // label_president
            // 
            this.label_president.AutoSize = true;
            this.label_president.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_president.Location = new System.Drawing.Point(183, 126);
            this.label_president.Name = "label_president";
            this.label_president.Size = new System.Drawing.Size(340, 20);
            this.label_president.TabIndex = 5;
            this.label_president.Text = "Who is the greatest President of all time?";
            this.label_president.Click += new System.EventHandler(this.Label_president_Click);
            // 
            // label_empty
            // 
            this.label_empty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_empty.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_empty.Location = new System.Drawing.Point(217, 172);
            this.label_empty.Name = "label_empty";
            this.label_empty.Size = new System.Drawing.Size(276, 37);
            this.label_empty.TabIndex = 6;
            this.label_empty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_answer
            // 
            this.btn_answer.AutoSize = true;
            this.btn_answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_answer.Location = new System.Drawing.Point(257, 232);
            this.btn_answer.Name = "btn_answer";
            this.btn_answer.Size = new System.Drawing.Size(180, 50);
            this.btn_answer.TabIndex = 7;
            this.btn_answer.Text = "Click to see Answer";
            this.btn_answer.UseVisualStyleBackColor = true;
            this.btn_answer.Click += new System.EventHandler(this.Btn_answer_Click);
            // 
            // label_goodMorning
            // 
            this.label_goodMorning.AutoSize = true;
            this.label_goodMorning.Location = new System.Drawing.Point(189, 309);
            this.label_goodMorning.Name = "label_goodMorning";
            this.label_goodMorning.Size = new System.Drawing.Size(334, 20);
            this.label_goodMorning.TabIndex = 8;
            this.label_goodMorning.Text = "Select a language and I will say Good Morning";
            // 
            // label_empty2
            // 
            this.label_empty2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_empty2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_empty2.Location = new System.Drawing.Point(217, 351);
            this.label_empty2.Name = "label_empty2";
            this.label_empty2.Size = new System.Drawing.Size(276, 40);
            this.label_empty2.TabIndex = 9;
            this.label_empty2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_italian
            // 
            this.btn_italian.Location = new System.Drawing.Point(31, 421);
            this.btn_italian.Name = "btn_italian";
            this.btn_italian.Size = new System.Drawing.Size(107, 54);
            this.btn_italian.TabIndex = 10;
            this.btn_italian.Text = "Italian";
            this.btn_italian.UseVisualStyleBackColor = true;
            this.btn_italian.Click += new System.EventHandler(this.Btn_italian_Click);
            // 
            // btn_spanish
            // 
            this.btn_spanish.Location = new System.Drawing.Point(307, 421);
            this.btn_spanish.Name = "btn_spanish";
            this.btn_spanish.Size = new System.Drawing.Size(107, 54);
            this.btn_spanish.TabIndex = 11;
            this.btn_spanish.Text = "Spanish";
            this.btn_spanish.UseVisualStyleBackColor = true;
            this.btn_spanish.Click += new System.EventHandler(this.Btn_spanish_Click);
            // 
            // German
            // 
            this.German.Location = new System.Drawing.Point(571, 421);
            this.German.Name = "German";
            this.German.Size = new System.Drawing.Size(107, 54);
            this.German.TabIndex = 12;
            this.German.Text = "German";
            this.German.UseVisualStyleBackColor = true;
            this.German.Click += new System.EventHandler(this.Button3_Click);
            // 
            // acePoker
            // 
            this.acePoker.Image = global::My_First_Project.Properties.Resources.acePoker;
            this.acePoker.Location = new System.Drawing.Point(370, 740);
            this.acePoker.Name = "acePoker";
            this.acePoker.Size = new System.Drawing.Size(100, 193);
            this.acePoker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.acePoker.TabIndex = 15;
            this.acePoker.TabStop = false;
            this.acePoker.Visible = false;
            this.acePoker.Click += new System.EventHandler(this.AcePoker_Click);
            // 
            // backCard
            // 
            this.backCard.Image = global::My_First_Project.Properties.Resources.backPoker;
            this.backCard.Location = new System.Drawing.Point(257, 740);
            this.backCard.Name = "backCard";
            this.backCard.Size = new System.Drawing.Size(100, 193);
            this.backCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backCard.TabIndex = 14;
            this.backCard.TabStop = false;
            this.backCard.Click += new System.EventHandler(this.PictureBox2_Click);
            // 
            // dragon
            // 
            this.dragon.Image = global::My_First_Project.Properties.Resources._38166844_dragon_wallpaper;
            this.dragon.Location = new System.Drawing.Point(193, 496);
            this.dragon.Name = "dragon";
            this.dragon.Size = new System.Drawing.Size(336, 238);
            this.dragon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dragon.TabIndex = 13;
            this.dragon.TabStop = false;
            this.dragon.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // btn_backPoker
            // 
            this.btn_backPoker.Location = new System.Drawing.Point(257, 939);
            this.btn_backPoker.Name = "btn_backPoker";
            this.btn_backPoker.Size = new System.Drawing.Size(107, 54);
            this.btn_backPoker.TabIndex = 16;
            this.btn_backPoker.Text = "Back of Card";
            this.btn_backPoker.UseVisualStyleBackColor = true;
            this.btn_backPoker.Click += new System.EventHandler(this.Btn_backPoker_Click);
            // 
            // btn_face
            // 
            this.btn_face.Location = new System.Drawing.Point(370, 939);
            this.btn_face.Name = "btn_face";
            this.btn_face.Size = new System.Drawing.Size(107, 54);
            this.btn_face.TabIndex = 17;
            this.btn_face.Text = "Front of Card";
            this.btn_face.UseVisualStyleBackColor = true;
            this.btn_face.Click += new System.EventHandler(this.Btn_face_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.Location = new System.Drawing.Point(307, 1021);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(107, 54);
            this.btn_exit.TabIndex = 19;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.Btn_exit_Click);
            // 
            // backPoker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 1087);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_face);
            this.Controls.Add(this.btn_backPoker);
            this.Controls.Add(this.acePoker);
            this.Controls.Add(this.backCard);
            this.Controls.Add(this.dragon);
            this.Controls.Add(this.German);
            this.Controls.Add(this.btn_spanish);
            this.Controls.Add(this.btn_italian);
            this.Controls.Add(this.label_empty2);
            this.Controls.Add(this.label_goodMorning);
            this.Controls.Add(this.btn_answer);
            this.Controls.Add(this.label_empty);
            this.Controls.Add(this.label_president);
            this.Controls.Add(this.clickButton1);
            this.Controls.Add(this.clickButton2);
            this.Controls.Add(this.messageButton);
            this.Name = "backPoker";
            this.Text = "My First Program";
            ((System.ComponentModel.ISupportInitialize)(this.acePoker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button messageButton;
        private System.Windows.Forms.Button clickButton2;
        private System.Windows.Forms.Button clickButton1;
        private System.Windows.Forms.Label label_president;
        private System.Windows.Forms.Label label_empty;
        private System.Windows.Forms.Button btn_answer;
        private System.Windows.Forms.Label label_goodMorning;
        private System.Windows.Forms.Label label_empty2;
        private System.Windows.Forms.Button btn_italian;
        private System.Windows.Forms.Button btn_spanish;
        private System.Windows.Forms.Button German;
        private System.Windows.Forms.PictureBox dragon;
        private System.Windows.Forms.PictureBox backCard;
        private System.Windows.Forms.PictureBox acePoker;
        private System.Windows.Forms.Button btn_backPoker;
        private System.Windows.Forms.Button btn_face;
        private System.Windows.Forms.Button btn_exit;
    }
}

