﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        private void Btn_openFile_Click(object sender, EventArgs e)
        {
            try
            {
                string getFile;
                StreamReader inputFile;
                inputFile = File.OpenText("Inventory.txt");

                lb_inventory.Items.Clear();

                while (!inputFile.EndOfStream)
                {
                    getFile = inputFile.ReadLine();

                    lb_inventory.Items.Add(getFile);
                }

                // Close the file
                inputFile.Close();
            }
            catch (Exception ex)
            {
                // Display an error message
                MessageBox.Show(ex.Message);
            }
        }

        private void L_add_Click(object sender, EventArgs e)
        {
            // Open form1
            Form1 frm = new Form1();
            frm.Show();
        }

        private void Btn_delete_Click(object sender, EventArgs e)
        {
            // Delete inventory
            if (lb_inventory.SelectedItems.Count != 0)
            {
                while (lb_inventory.SelectedIndex != -1)
                {
                    lb_inventory.Items.RemoveAt(lb_inventory.SelectedIndex);
                }
            }
        }

        private void Btn_save_Click(object sender, EventArgs e)
        {
            // StreamWriter to update the file if Chosen to delete invetory
            if (lb_inventory.SelectedIndex > -1)
            {
                lb_inventory.Items.Remove(lb_inventory.SelectedItem);
                using (FileStream fs = new FileStream("Inventory.txt", FileMode.Create, FileAccess.Write))
                {
                    using (TextWriter tw = new StreamWriter(fs))
                        foreach (string item in lb_inventory.Items)
                            tw.WriteLine(item);
                }
            }

            // Display a Messagebox
            MessageBox.Show("The Inventory has been updated and saved");
        }

        private void Btn_exit2_Click(object sender, EventArgs e)
        {
            // Exit the form
            this.Close();
        }
    }
}
   


