﻿namespace Milestone2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_inventory = new System.Windows.Forms.ListBox();
            this.l_add = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_exit2 = new System.Windows.Forms.Button();
            this.btn_openFile = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_inventory
            // 
            this.lb_inventory.FormattingEnabled = true;
            this.lb_inventory.ItemHeight = 16;
            this.lb_inventory.Location = new System.Drawing.Point(13, 13);
            this.lb_inventory.Name = "lb_inventory";
            this.lb_inventory.Size = new System.Drawing.Size(621, 388);
            this.lb_inventory.TabIndex = 0;
            // 
            // l_add
            // 
            this.l_add.AutoSize = true;
            this.l_add.Location = new System.Drawing.Point(648, 59);
            this.l_add.Name = "l_add";
            this.l_add.Size = new System.Drawing.Size(121, 27);
            this.l_add.TabIndex = 1;
            this.l_add.Text = "Add Inventory";
            this.l_add.UseVisualStyleBackColor = true;
            this.l_add.Click += new System.EventHandler(this.L_add_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.AutoSize = true;
            this.btn_delete.Location = new System.Drawing.Point(648, 145);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(121, 27);
            this.btn_delete.TabIndex = 2;
            this.btn_delete.Text = "Delete Inventory";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.Btn_delete_Click);
            // 
            // btn_exit2
            // 
            this.btn_exit2.Location = new System.Drawing.Point(648, 189);
            this.btn_exit2.Name = "btn_exit2";
            this.btn_exit2.Size = new System.Drawing.Size(121, 23);
            this.btn_exit2.TabIndex = 3;
            this.btn_exit2.Text = "Exit";
            this.btn_exit2.UseVisualStyleBackColor = true;
            this.btn_exit2.Click += new System.EventHandler(this.Btn_exit2_Click);
            // 
            // btn_openFile
            // 
            this.btn_openFile.Location = new System.Drawing.Point(648, 13);
            this.btn_openFile.Name = "btn_openFile";
            this.btn_openFile.Size = new System.Drawing.Size(121, 23);
            this.btn_openFile.TabIndex = 4;
            this.btn_openFile.Text = "Open File";
            this.btn_openFile.UseVisualStyleBackColor = true;
            this.btn_openFile.Click += new System.EventHandler(this.Btn_openFile_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(648, 102);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(121, 23);
            this.btn_save.TabIndex = 5;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.Btn_save_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_openFile);
            this.Controls.Add(this.btn_exit2);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.l_add);
            this.Controls.Add(this.lb_inventory);
            this.Name = "Form2";
            this.Text = "View Inventory";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lb_inventory;
        private System.Windows.Forms.Button l_add;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_exit2;
        private System.Windows.Forms.Button btn_openFile;
        private System.Windows.Forms.Button btn_save;
    }
}