﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private Inventory product = null;


        private void Btn_save_Click(object sender, EventArgs e)
        {
            // Create a StreamWriter to create text that displays the Product name, sku, brand and etc...
            StreamWriter newFile;
            newFile = File.AppendText("Inventory.txt");          
            newFile.WriteLine(tb_name.Text + "\t   " + "\t  " + tb_sku.Text + "\t   " +  tb_brand.Text + "\t   " +  tb_qnty.Text + "\t   " +  tb_retail.Text + "\t   " +  tb_price.Text);
            newFile.Close();

            

            // Display a Messagebox
            MessageBox.Show("The Inventory has been updated and saved");
        }

        private void Btn_view_Click(object sender, EventArgs e)
        {
            // Open the new form
            Form2 frm = new Form2();
            frm.Show();
        }

        private void Btn_exit_Click(object sender, EventArgs e)
        {
            // Exith the form
            this.Close();
        }
    }
}
