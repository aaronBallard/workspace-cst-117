﻿namespace Milestone5
{
    partial class guitarMetroPalaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_guitarMetroPalace = new System.Windows.Forms.Label();
            this.btn_viewInventory = new System.Windows.Forms.Button();
            this.btn_addInventory = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // l_guitarMetroPalace
            // 
            this.l_guitarMetroPalace.AutoSize = true;
            this.l_guitarMetroPalace.BackColor = System.Drawing.Color.Black;
            this.l_guitarMetroPalace.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_guitarMetroPalace.ForeColor = System.Drawing.Color.Red;
            this.l_guitarMetroPalace.Location = new System.Drawing.Point(130, 46);
            this.l_guitarMetroPalace.Name = "l_guitarMetroPalace";
            this.l_guitarMetroPalace.Size = new System.Drawing.Size(326, 38);
            this.l_guitarMetroPalace.TabIndex = 0;
            this.l_guitarMetroPalace.Text = "Guitar Metro Palace";
            // 
            // btn_viewInventory
            // 
            this.btn_viewInventory.BackColor = System.Drawing.Color.Aqua;
            this.btn_viewInventory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_viewInventory.Location = new System.Drawing.Point(193, 107);
            this.btn_viewInventory.Name = "btn_viewInventory";
            this.btn_viewInventory.Size = new System.Drawing.Size(208, 43);
            this.btn_viewInventory.TabIndex = 1;
            this.btn_viewInventory.Text = "View Inventory";
            this.btn_viewInventory.UseVisualStyleBackColor = false;
            this.btn_viewInventory.Click += new System.EventHandler(this.Btn_viewInventory_Click);
            // 
            // btn_addInventory
            // 
            this.btn_addInventory.BackColor = System.Drawing.Color.Aqua;
            this.btn_addInventory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_addInventory.Location = new System.Drawing.Point(193, 171);
            this.btn_addInventory.Name = "btn_addInventory";
            this.btn_addInventory.Size = new System.Drawing.Size(208, 43);
            this.btn_addInventory.TabIndex = 2;
            this.btn_addInventory.Text = "Add Inventory";
            this.btn_addInventory.UseVisualStyleBackColor = false;
            this.btn_addInventory.Click += new System.EventHandler(this.Btn_addInventory_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.BackColor = System.Drawing.Color.Aqua;
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.Location = new System.Drawing.Point(193, 237);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(208, 43);
            this.btn_exit.TabIndex = 3;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = false;
            this.btn_exit.Click += new System.EventHandler(this.Btn_exit_Click);
            // 
            // guitarMetroPalaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Milestone5.Properties.Resources.gis_guitars;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(610, 352);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_addInventory);
            this.Controls.Add(this.btn_viewInventory);
            this.Controls.Add(this.l_guitarMetroPalace);
            this.Name = "guitarMetroPalaceForm";
            this.Text = "Guitar Metro Palace";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_guitarMetroPalace;
        private System.Windows.Forms.Button btn_viewInventory;
        private System.Windows.Forms.Button btn_addInventory;
        private System.Windows.Forms.Button btn_exit;
    }
}

