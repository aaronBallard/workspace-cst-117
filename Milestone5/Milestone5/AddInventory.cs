﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone5
{
    public partial class AddInventory : Form
    {
        public AddInventory()
        {
            InitializeComponent();
        }

        private void Btn_add_Click(object sender, EventArgs e)
        {
            // Create a StreamWriter to create text that displays the Product name, sku, brand and etc...
            StreamWriter newFile;
            newFile = File.AppendText("Inventory.txt");
            newFile.WriteLine(tb_name.Text + "\t" + "\t" + tb_sku.Text + "\t" + "\t" + tb_brand.Text + "\t" + tb_qnty.Text + "\t" + tb_retail.Text + "\t " + tb_price.Text);
            newFile.Close();

            // Display a Messagebox
            MessageBox.Show("The Inventory has been updated and saved");

            // Clear the TextBox controls
            tb_name.Clear();
            tb_sku.Clear();
            tb_brand.Clear();
            tb_qnty.Clear();
            tb_retail.Clear();
            tb_price.Clear();

            // Reset the focus
            tb_name.Focus();
        }

        private void Btn_view_Click(object sender, EventArgs e)
        {
            // Open the View Inventory form
            ViewInventory viewInventory = new ViewInventory();
            viewInventory.Show();
        }

        private void Btn_exit_Click(object sender, EventArgs e)
        {
            // Exit the form
            this.Close();
        }
    }
}
