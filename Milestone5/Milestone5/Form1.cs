﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone5
{
    public partial class guitarMetroPalaceForm : Form
    {
        public guitarMetroPalaceForm()
        {
            InitializeComponent();
        }

        private void Btn_viewInventory_Click(object sender, EventArgs e)
        {
            // Open the View Inventory form
            ViewInventory viewInventory = new ViewInventory();
            viewInventory.Show();
        }

        private void Btn_addInventory_Click(object sender, EventArgs e)
        {
            // Open the Add Inventory form
            AddInventory addInventory = new AddInventory();
            addInventory.Show();
        }

        private void Btn_exit_Click(object sender, EventArgs e)
        {
            // Exit the form
            this.Close();
        }
    }
}
