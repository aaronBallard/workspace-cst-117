﻿namespace Milestone5
{
    partial class AddInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_productName = new System.Windows.Forms.Label();
            this.l_sku = new System.Windows.Forms.Label();
            this.l_brand = new System.Windows.Forms.Label();
            this.l_qnty = new System.Windows.Forms.Label();
            this.l_retailPrice = new System.Windows.Forms.Label();
            this.l_price = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_sku = new System.Windows.Forms.TextBox();
            this.tb_brand = new System.Windows.Forms.TextBox();
            this.tb_qnty = new System.Windows.Forms.TextBox();
            this.tb_retail = new System.Windows.Forms.TextBox();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_view = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // l_productName
            // 
            this.l_productName.AutoSize = true;
            this.l_productName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.l_productName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_productName.ForeColor = System.Drawing.Color.Red;
            this.l_productName.Location = new System.Drawing.Point(25, 70);
            this.l_productName.Name = "l_productName";
            this.l_productName.Size = new System.Drawing.Size(155, 25);
            this.l_productName.TabIndex = 0;
            this.l_productName.Text = "Product Name:";
            // 
            // l_sku
            // 
            this.l_sku.AutoSize = true;
            this.l_sku.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.l_sku.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_sku.ForeColor = System.Drawing.Color.Red;
            this.l_sku.Location = new System.Drawing.Point(104, 130);
            this.l_sku.Name = "l_sku";
            this.l_sku.Size = new System.Drawing.Size(76, 25);
            this.l_sku.TabIndex = 1;
            this.l_sku.Text = "SKU#:";
            // 
            // l_brand
            // 
            this.l_brand.AutoSize = true;
            this.l_brand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.l_brand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_brand.ForeColor = System.Drawing.Color.Red;
            this.l_brand.Location = new System.Drawing.Point(104, 190);
            this.l_brand.Name = "l_brand";
            this.l_brand.Size = new System.Drawing.Size(76, 25);
            this.l_brand.TabIndex = 2;
            this.l_brand.Text = "Brand:";
            // 
            // l_qnty
            // 
            this.l_qnty.AutoSize = true;
            this.l_qnty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.l_qnty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_qnty.ForeColor = System.Drawing.Color.Red;
            this.l_qnty.Location = new System.Drawing.Point(80, 250);
            this.l_qnty.Name = "l_qnty";
            this.l_qnty.Size = new System.Drawing.Size(100, 25);
            this.l_qnty.TabIndex = 3;
            this.l_qnty.Text = "Quantity:";
            // 
            // l_retailPrice
            // 
            this.l_retailPrice.AutoSize = true;
            this.l_retailPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.l_retailPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_retailPrice.ForeColor = System.Drawing.Color.Red;
            this.l_retailPrice.Location = new System.Drawing.Point(52, 310);
            this.l_retailPrice.Name = "l_retailPrice";
            this.l_retailPrice.Size = new System.Drawing.Size(128, 25);
            this.l_retailPrice.TabIndex = 4;
            this.l_retailPrice.Text = "Retail Price:";
            // 
            // l_price
            // 
            this.l_price.AutoSize = true;
            this.l_price.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.l_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_price.ForeColor = System.Drawing.Color.Red;
            this.l_price.Location = new System.Drawing.Point(69, 370);
            this.l_price.Name = "l_price";
            this.l_price.Size = new System.Drawing.Size(111, 25);
            this.l_price.TabIndex = 5;
            this.l_price.Text = "Sell Price:";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(202, 72);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(370, 22);
            this.tb_name.TabIndex = 6;
            // 
            // tb_sku
            // 
            this.tb_sku.Location = new System.Drawing.Point(202, 132);
            this.tb_sku.Name = "tb_sku";
            this.tb_sku.Size = new System.Drawing.Size(370, 22);
            this.tb_sku.TabIndex = 7;
            // 
            // tb_brand
            // 
            this.tb_brand.Location = new System.Drawing.Point(202, 192);
            this.tb_brand.Name = "tb_brand";
            this.tb_brand.Size = new System.Drawing.Size(370, 22);
            this.tb_brand.TabIndex = 8;
            // 
            // tb_qnty
            // 
            this.tb_qnty.Location = new System.Drawing.Point(202, 252);
            this.tb_qnty.Name = "tb_qnty";
            this.tb_qnty.Size = new System.Drawing.Size(370, 22);
            this.tb_qnty.TabIndex = 9;
            // 
            // tb_retail
            // 
            this.tb_retail.Location = new System.Drawing.Point(202, 312);
            this.tb_retail.Name = "tb_retail";
            this.tb_retail.Size = new System.Drawing.Size(370, 22);
            this.tb_retail.TabIndex = 10;
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(202, 372);
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(370, 22);
            this.tb_price.TabIndex = 11;
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.Red;
            this.btn_add.Location = new System.Drawing.Point(193, 414);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(120, 51);
            this.btn_add.TabIndex = 12;
            this.btn_add.Text = "Add";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.Btn_add_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.ForeColor = System.Drawing.Color.Red;
            this.btn_exit.Location = new System.Drawing.Point(445, 414);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(120, 51);
            this.btn_exit.TabIndex = 13;
            this.btn_exit.Text = "Close";
            this.btn_exit.UseVisualStyleBackColor = false;
            this.btn_exit.Click += new System.EventHandler(this.Btn_exit_Click);
            // 
            // btn_view
            // 
            this.btn_view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_view.ForeColor = System.Drawing.Color.Red;
            this.btn_view.Location = new System.Drawing.Point(319, 414);
            this.btn_view.Name = "btn_view";
            this.btn_view.Size = new System.Drawing.Size(120, 51);
            this.btn_view.TabIndex = 14;
            this.btn_view.Text = "View";
            this.btn_view.UseVisualStyleBackColor = false;
            this.btn_view.Click += new System.EventHandler(this.Btn_view_Click);
            // 
            // AddInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Milestone5.Properties.Resources.electric_guitar;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(633, 477);
            this.Controls.Add(this.btn_view);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.tb_price);
            this.Controls.Add(this.tb_retail);
            this.Controls.Add(this.tb_qnty);
            this.Controls.Add(this.tb_brand);
            this.Controls.Add(this.tb_sku);
            this.Controls.Add(this.tb_name);
            this.Controls.Add(this.l_price);
            this.Controls.Add(this.l_retailPrice);
            this.Controls.Add(this.l_qnty);
            this.Controls.Add(this.l_brand);
            this.Controls.Add(this.l_sku);
            this.Controls.Add(this.l_productName);
            this.Name = "AddInventory";
            this.Text = "AddInventory";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_productName;
        private System.Windows.Forms.Label l_sku;
        private System.Windows.Forms.Label l_brand;
        private System.Windows.Forms.Label l_qnty;
        private System.Windows.Forms.Label l_retailPrice;
        private System.Windows.Forms.Label l_price;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_sku;
        private System.Windows.Forms.TextBox tb_brand;
        private System.Windows.Forms.TextBox tb_qnty;
        private System.Windows.Forms.TextBox tb_retail;
        private System.Windows.Forms.TextBox tb_price;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_view;
    }
}