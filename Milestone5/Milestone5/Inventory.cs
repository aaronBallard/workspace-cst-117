﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone5
{
    class Inventory
    {
        // Private instance variables
        private string name;
        private string sku;
        private string brand;
        private string qnty;
        private string retail;
        private string price;

        // Constructor
        public Inventory()
        {

        }

        // Constructor with private variables
        public Inventory(string name, string sku, string brand, string qnty, string retail, string price)
        {
            this.name = name;
            this.sku = sku;
            this.brand = brand;
            this.qnty = qnty;
            this.retail = retail;
            this.price = price;
        }

        // Set and get methods
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Sku
        {
            get
            {
                return sku;
            }
            set
            {
                sku = value;
            }
        }
        public string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
            }
        }
        public string Qnty
        {
            get
            {
                return qnty;
            }
            set
            {
                qnty = value;
            }
        }
        public string Retail
        {
            get
            {
                return retail;
            }
            set
            {
                retail = value;
            }
        }
        public string Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }

        // Display a return message
        public string GetDisplayText(string sep)
        {
            return name + sep + sku + sep + brand + sep + qnty + sep + retail + sep + price;
        }

    }
}

